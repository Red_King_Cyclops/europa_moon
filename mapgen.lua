--minetest.clear_registered_ores()
minetest.clear_registered_biomes()
minetest.clear_registered_decorations()
minetest.override_item("default:mossycobble", {tiles = {"default_ice.png"}})

--ice crust biome
	minetest.register_biome({
		name = "ice_crust",
		--node_dust = "default:gravel",
		node_top = "default:gravel",
		depth_top = 1,
		node_filler = "default:ice",
		depth_filler = 3,
		node_stone = "default:ice",
		node_water_top = "air",
		depth_water_top =1 ,
		node_water = "air",
		node_river_water = "air",
		y_min = -1033,
		y_max = 500,
		heat_point = 10,
		humidity_point = 0,
	})

--ice spikes biome (part of ice crust)
	minetest.register_biome({
		name = "ice_spikes",
		--node_dust = "default:gravel",
		node_top = "default:gravel",
		depth_top = 1,
		node_filler = "default:ice",
		depth_filler = 3,
		node_stone = "default:ice",
		node_water_top = "air",
		depth_water_top =1 ,
		node_water = "air",
		node_river_water = "air",
		y_min = -1033,
		y_max = 500,
		heat_point = 0,
		humidity_point = 50,
	})

--ocean biome
	minetest.register_biome({
		name = "europa_ocean",
		--node_dust = "default:gravel",
		node_top = "default:water_source",
		depth_top = 1,
		node_filler = "default:water_source",
		depth_filler = 3,
		node_stone = "default:water_source",
		node_water_top = "default:water_source",
		depth_water_top =1 ,
		node_water = "default:water_source",
		node_river_water = "default:water_source",
		y_min = -3616,
		y_max = -1034,
		heat_point = 25,
		humidity_point = 100,
	})

--rocky mantle biome
	minetest.register_biome({
		name = "rocky_mantle",
		--node_dust = "default:gravel",
		node_top = "default:obsidian",
		depth_top = 5,
		node_filler = "default:stone",
		depth_filler = 3,
		node_stone = "default:stone",
		node_water_top = "air",
		depth_water_top =1 ,
		node_water = "air",
		node_river_water = "air",
		y_min = -22216,
		y_max = -3616,
		heat_point = 50,
		humidity_point = 0,
	})

--iron core biome
	minetest.register_biome({
		name = "iron_core",
		--node_dust = "default:gravel",
		node_top = "default:steelblock",
		depth_top = 1,
		node_filler = "default:steelblock",
		depth_filler = 3,
		node_stone = "default:steelblock",
		node_water_top = "air",
		depth_water_top =1 ,
		node_water = "air",
		node_river_water = "air",
		y_min = -31000,
		y_max = -22217,
		heat_point = 100,
		humidity_point = 0,
	})
	
--creates the reddish streaks
	minetest.register_ore({
		ore_type       = "vein",
		ore            = "europa_moon:red_rocks",
		wherein        = "default:gravel",
		--clust_scarcity = 9 * 9 * 9,
		--clust_num_ores = 12,
		--clust_size     = 3,
		y_min          = -1033,
		y_max          = 500,
		biomes         = {"ice_crust", "ice_spikes"},
		noise_threshold = 0.5,
		noise_params = {
            offset = 0,
            scale = 1,
            spread = {x = 100, y = 100, z = 100},
            seed = 23,
            octaves = 3,
            persist = 0.7
		},
		random_factor = 0.01, --1.0,
	})

--creates igneous ocean floor
	minetest.register_ore({
		ore_type       = "stratum",
		ore            = "default:obsidian",
		wherein        = "default:stone",
		--clust_scarcity = 9 * 9 * 9,
		--clust_num_ores = 12,
		--clust_size     = 3,
		y_min          = -3621,
		y_max          = -3616,
		biomes         = {"rocky_mantle", "europa_ocean"},
        np_stratum_thickness = {
            offset = 8,
            scale = 4,
            spread = {x = 100, y = 100, z = 100},
            seed = 17,
            octaves = 3,
            persist = 0.7
        },
stratum_thickness = 5,
	})

--creates the lava on the sea floor
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "europa_moon:ocean_lava",
		wherein        = "default:obsidian",
		clust_scarcity = 5 * 5 * 5,
		clust_num_ores = 12,
		clust_size     = 3,
		y_min          = -3800,
		y_max          = -3600,
	})
	
--creates lava throughout the mantle
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "default:lava_source",
		wherein        = "default:obsidian",
		clust_scarcity = 5 * 5 * 5,
		clust_num_ores = 12,
		clust_size     = 3,
		y_min          = -22216,
		y_max          = -3801,
	})

--creates the ice spikes
	minetest.register_decoration({
		deco_type = "schematic",
		place_on = {"default:gravel", "europa_moon:red_rocks"},
		sidelen = 7,
		--[[
		noise_params = {
			offset = 0.036,
			scale = 0.022,
			spread = {x = 250, y = 250, z = 250},
			seed = 2,
			octaves = 3,
			persist = 0.66
		},
		]]
		fill_ratio = 0.02,
		biomes = {"ice_spikes"},
		y_min = -500,
		y_max = 500,
		schematic = minetest.get_modpath("europa_moon") .. "/schematics/ice_spike.mts",
		flags = "place_center_x, place_center_z",
		rotation = "random",
	})

--for spaceship wrecks I plan to use this if I use fill_ratio:
--		fill_ratio = 0.00002,