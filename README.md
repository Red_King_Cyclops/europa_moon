# Europa Moon
![Mountain on Europa](https://i.ibb.co/5nXcP61/screenshot-20190622-161805.png)
This mod replaces the default Minetest world with Europa, a moon of Jupiter.

A world created with this mod has four layers: ice crust, ocean, rocky mantle, and iron core. The ice crust is the uppermost layer and has ice instead of stone. The ocean is beneath the ice and consists of just water. The rocky mantle is beneath the ocean and contains all the ores from default. The iron core is beneath the rocky mantle and has steel blocks instead of stone. To get a good idea of Europa's composition, look [here](https://www.google.com/search?q=composition+of+europa&source=lnms&tbm=isch&sa=X&ved=0ahUKEwi9zqzHwv3iAhVLmeAKHfqdAl4Q_AUIECgB&biw=1920&bih=949#imgrc=7Zk0KJtKcp4xZM:).
![Europa's interior](https://theblogofthecosmos.files.wordpress.com/2015/08/europa_poster-svg.png)

Like my other mod, saturn_moon, dungeons are now covered with ice.

Unlike saturn_moon, I intend to do more with this mod. There is a large amount of potential content that comes with a setting on Europa, such as alien life forms that live in the ocean of Europa. Currently, this mod is a work in progress. For example, the volcanic vents on the bottom of the ocean have not been fully added yet, there are no aliens yet, and the surface does not yet have the craters.

The code is licenced under LGPL 2.1 and all the textures, except for the Jupiter image, are licenced under CC-BY-SA. The Jupiter image is taken from https://www.flickr.com/photos/nasahubble/46616136772 , which is licenced under CC-BY-2.0. The red rocks texture is a modified version of the gravel image from default.