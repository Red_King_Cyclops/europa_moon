minetest.register_node("europa_moon:red_rocks", {
    description = "Reddish Rocks",
    tiles = {"europa_moon_red_rocks.png"},
    --is_ground_content = true,
    groups = {crumbly=1, falling_node = 1},
    sounds = default.node_sound_gravel_defaults()
})

minetest.register_node("europa_moon:ocean_lava", {
	description = "Ocean Lava",
	tiles = {
		{
			name = "default_lava_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
	},
	--paramtype = "light",
	light_source = default.LIGHT_MAX - 1,
	walkable = false,
	--pointable = true,
	--diggable = true,
	--buildable_to = true,
	--is_ground_content = false,
	--drop = "",
	groups = {cracky=3},
	damage_per_second = 4 * 2,
})
